import { TestBed } from '@angular/core/testing';

import { AuthLoginGuardService } from './auth-login-guard.service';

describe('AuthLoginGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthLoginGuardService = TestBed.get(AuthLoginGuardService);
    expect(service).toBeTruthy();
  });
});
