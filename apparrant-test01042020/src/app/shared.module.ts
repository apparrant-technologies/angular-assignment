import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import {
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatCardModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatOptionModule,
    MatListModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatPaginatorModule
} from '@angular/material';


@NgModule({
    declarations: [
    ],
    imports: [
        MatToolbarModule,
        MatInputModule,
        MatCardModule,
        MatMenuModule,
        MatIconModule,
        MatButtonModule,
        MatDividerModule,
        MatSlideToggleModule,
        MatSelectModule,
        MatOptionModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatAutocompleteModule,
        MatSnackBarModule,
        MatExpansionModule,
        MatPaginatorModule
    ],
    exports: [
        MatToolbarModule,
        MatInputModule,
        MatCardModule,
        MatMenuModule,
        MatIconModule,
        MatButtonModule,
        MatDividerModule,
        MatSlideToggleModule,
        MatSelectModule,
        MatOptionModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatAutocompleteModule,
        MatSnackBarModule,
        MatExpansionModule,
        MatPaginatorModule
    ]
})
export class SharedModule { }