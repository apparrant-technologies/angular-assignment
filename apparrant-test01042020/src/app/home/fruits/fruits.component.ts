import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar'
import { map, startWith } from 'rxjs/operators';
import { SnackComponent } from '../snack/snack.component';

@Component({
  selector: 'app-fruits',
  templateUrl: './fruits.component.html',
  styleUrls: ['./fruits.component.scss']
})
export class FruitsComponent implements OnInit {

  public limit = 10;
  public offset = 0;
  public next = 10;

  public orgfruitsList: any = {
    description: null,
    fruits: []
  };

  public fruitsList: any;

  public myControl = new FormControl();
  public filteredOptions: Observable<string[]>;

  constructor(
    private api: ApiService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getFruits();
    this.initFilterFruitVal();
  }

  initFilterFruitVal() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  getFruits() {
    this.api.get(`/assets/resources/fruits.json`, "").subscribe((res) => {
      this.orgfruitsList = res;
      this.fruitsList = this.orgfruitsList;
    },
      error => {

      })
  }

  openSnackBar() {
    
    this._snackBar.openFromComponent(SnackComponent, {
      data:this.myControl.value,
      duration: 5000,
      verticalPosition: 'top',
      horizontalPosition: 'end',
    });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    this.fruitsList = {
      fruits: this.orgfruitsList.fruits.filter(option => option.toLowerCase().includes(filterValue))
    };
    return this.fruitsList.fruits;
  }

  handlePageChange(event) {
    this.limit = event.pageSize;
    this.offset = event.pageIndex * event.pageSize;
    this.next = this.limit + this.offset;
  }
}
