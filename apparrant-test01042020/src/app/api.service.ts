import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Http, Response } from '@angular/http';
import { environment } from '../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    public http: Http,
    public httpClient: HttpClient,
  ) { }


  get(scriptName: string, param: string) {

    return this.http.get(environment.url + scriptName + "?" + param).
      pipe(map((res: Response) => { return res.json() }));
  }

}
