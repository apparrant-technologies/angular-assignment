import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { FruitsComponent } from './fruits/fruits.component';
import { VegetablesComponent } from './vegetables/vegetables.component';
import { SharedModule } from '../shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SnackComponent } from './snack/snack.component';
import {MAT_SNACK_BAR_DATA} from '@angular/material';

@NgModule({
  declarations: [HomeComponent, FruitsComponent, VegetablesComponent, SnackComponent],
  providers: [ { provide: MAT_SNACK_BAR_DATA, useValue: {} }],
  entryComponents: [SnackComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ]
})
export class HomeModule { }
