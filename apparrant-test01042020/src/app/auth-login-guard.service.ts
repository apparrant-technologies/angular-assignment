import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthLoginGuardService {

  constructor(public authService: AuthService, public router: Router) { }

  async canActivate() {
    if (await this.authService.checkAuthenticated()) {
      await this.router.navigate(['home']);
      return false;
    }
    return true;
  }
}
