import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ObjectService {

  public isLoader = true;

  constructor(private router: Router) {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        // Function you want to call here
        this.hideLoader();
      }
    });
  }

  // to show loader is working :: intentionally delayed
  hideLoader() {
    setTimeout(() => {
      this.isLoader = false;
    }, 2000);
  }

}
