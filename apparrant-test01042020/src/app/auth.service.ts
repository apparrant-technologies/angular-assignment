import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private authClient = ()=>{
    let _token = window.localStorage.getItem("token");
    if(_token){return true;}
    return false;
  };

  private signIn = ({username, password})=>{
    if(username!=="apparrant123"){
      return {status:403,message:"UserName is Incorrect"};
    }
    if(password!=="Apparrant@123"){
      return {status:403,message:"Password is Incorrect"};
    }
    window.localStorage.setItem("token","1");
    return {status:200,message:"Password is Incorrect"};;
  }

  public isAuthenticated = new BehaviorSubject<boolean>(false);

  constructor(private router: Router) { }

  async checkAuthenticated() {
    const authenticated = await this.authClient();
    this.isAuthenticated.next(authenticated);
    return authenticated;
  }

  async login(username: string, password: string) {
    const transaction = await this.signIn({username, password});
    if(transaction.status!==200){
      throw Error(transaction.message);
    }

    if(transaction.status==200){
      this.router.navigateByUrl('/home');
    }
  }

  async logout(redirect: string) {
    try {
      this.clearToken();
      this.router.navigate([redirect]);
    } catch (err) {
      console.error(err);
    }
  }
  
  clearToken(){
    localStorage.clear();
  }
}
