import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-vegetables',
  templateUrl: './vegetables.component.html',
  styleUrls: ['./vegetables.component.scss']
})
export class VegetablesComponent implements OnInit {

  public vegetablesList:any;

  constructor(
    private api: ApiService
  ) { }

  ngOnInit() {
    this.getvegetables();
  }

  getvegetables() {
    this.api.get(`/assets/resources/vegetables.json`, "").subscribe((res) => {
      this.vegetablesList = res;
    },
    error => {

    })
  }


}
