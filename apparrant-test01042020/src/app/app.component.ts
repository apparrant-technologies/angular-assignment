import { Component } from '@angular/core';
import { ObjectService } from './object.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'apparrant-test01042020';

  constructor(public object:ObjectService){

  }
}
